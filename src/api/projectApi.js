import http from 'vue-resource'

export const projectApi = {
    getProjects() {
      return http.get('/api/projects')
    }
}

var store = {
  debug: true,
  state: {
    message: 'Hello!'
  },
}
