// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import VueCharts from  'vue-charts'
import DrAssist from './utils/assist'
import VueProgressBar from 'vue-progressbar'

Vue.config.productionTip = false

Vue.use(VueResource);
Vue.use(VueProgressBar, {
  color: '#36D7B7',
  failedColor: '#E43A45',
  thickness: '3px',
  transition: {
    speed: '0.2s',
    opacity: '0.6s',
    termination: 300
  },
});
Vue.use(VueCharts);
Vue.use(DrAssist);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
