import Vue from 'vue'
import Router from 'vue-router'
import News from '@/components/news'
import Project from '@/components/projects'
import NewProject from '@/components/projects/new-project'
import ProjectDetails from '@/components/projects/project-details'
import Summaries from '@/components/summries'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/about',
      name: 'News',
      component: News
    },
    {
      path: '/',
      name: 'Projects',
      component: Project
    },
    {
      path: '/projects',
      name: 'Projects',
      component: Project
    },
    {
      path: '/projects/new',
      name: 'NewProject',
      component: NewProject
    },
    {
      path: '/projects/:projectId',
      name: 'ProjectsDetails',
      component: ProjectDetails
    },
    {
      path: '/projects/:projectId/summaries/:summaryId',
      name: 'Summary',
      component: Summaries
    }
  ]
})
