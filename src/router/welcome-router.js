import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/main-page'
import Registration from '@/components/registration'
import PasswordReset from '@/components/account/password/reset'
import SimpleRegistration from '@/components/account/registration/simple'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/registration/:action?/:state?',
      name: 'Registration',
      component: Registration
    },
    {
      path: '/account/password/reset',
      name: 'PasswordReset',
      component: PasswordReset
    },
    {
      path: '/account/registration/simple',
      name: 'SimpleRegistration',
      component: SimpleRegistration
    }
  ]
})
