function install(Vue, options = {}) {
  Vue.component('percent-painter', {
    props: ['number'],
    template: '<span :class="color"><span v-if="this.number > 0">+</span>{{smartNumber}} %</span>',
    computed: {
      color() {
        return this.number > 0 ? 'number-up' : 'number-down';
      },
      smartNumber() {
        const scale = 2;
        let newNumber = this.number.toFixed(scale);

        for (let i = scale; i > 0; --i) {
          const power = Math.pow(10, i);
          if (newNumber * power % 10 != 0) {
            break;
          }
          newNumber = newNumber * power / power
        }

        const number = newNumber;

        return number;
      }
    }
  });
}

export default install
