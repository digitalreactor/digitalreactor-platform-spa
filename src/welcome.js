import Vue from 'vue'
import VueResource from 'vue-resource'
import router from './router/welcome-router'
import Welcome from './Welcome.vue'

Vue.config.productionTip = false;
Vue.use(VueResource);

new Vue({
  el: '#welcome',
  router,
  template: '<Welcome/>',
  components: { Welcome }
});
